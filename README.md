Tous les scripts sont dans le dossier Script rangés par objectif (à mettre en relation avec REPET, ou pour l'analyse des résultats)


La documentaion a été faite sur Zim (http://www.zim-wiki.org/), la présentation sera plus agréable qu’ici (images, liens clicables)…


chronologie


REPET fait sur


B.rapa sans aucun traitement préalable → OK

B.oleracea sans traitement préalable → problème dans le BLASTER

Blaster 185 → batch 244 → identification de la séquence à problème (séquence répétée)	résolution : masquage des séquences répétées avec MEME DUST avec un seuil de 100

B.oleracea_mask : B.oleracea masqué par DUST avec un seuil de 100	Tedenovo → OK	Teannot → OK (je l’ai relancé sur une nouvelle machine et cette fois, il a fonctionné)

B.napus_mask : B.napus masqué par DUST avec un seuil de 100 → problème dans le BLASTER	Batch 2, 453, 456, 457 → identification de la séquence à problème (séquence répétée)	résolution : masquage des séquences répétées avec MEME DUST avec un seuil de 30

B.rapa_100 : B.rapa masqué par DUST avec un seuil de 100 dans le but de voir si le masquage nous fait predre de l’information	Arret prématuré puisque le Bnapus masqué à 100 ne fonctionnait pas	TODO à finir

B.rapa_30 : B.rapa masqué par DUST avec un seuil de 30 dans le but de voir si le masquage nous fait predre de l’information	Un trop fort masquage entraine un nouvelle erreur de REPET avec Mreps. Nous l’avons contournée en sautant cette étape

B.rapa _wo_centro : B.rapa sans centromère dans le but de voir si leur retrait nous fait predre de l’information

La perte des centromères et des scaffolds diminue fortement la taille de la base de données créée par REPET. On conserve plus d’ETs en retirant les centromères qu’en masquant fortement le génome.

Bnapus sans aucun traitement préalable → pas glop, il y a environ 10 Blasters qui ne fonctionnent pas donc au moins 10 séquences problématiques mais il peut y en avoir plus.

Compromis : On fait un masquage léger du génome avec DUST à un seuil de 100 et on retire manuellement les séquences problématiques.

Bn_m_100_wo_rep : Bnapus masqué avec DUST à un seuil de 100 sans les séquences à problèmes → pas glop, explosion de la mémoire sur le PILLER	mais les jobs ne se faisaient qu’un par un	TODO à refaire à la fin des autres REPETs

Bn_m_100_wo_r_c : Bnapus masqué avec DUST à un seuil de 100 sans les séquences à problèmes ni les centromères	Tedenovo → OK	Teannot →OK

On retrouve deux types d’éléments transposables annotés

ceux annotés par Repbase	A01 Brapa_REPET_blastx match 8928901 8929428 0.0 – . ID=ms32758_A01_Copia-63_BRa-I_1p:ClassI:LTR:Copia;Target=Copia-63_BRa-I_1p:ClassI:LTR:Copia 425 599;TargetLength=1265;Identity=73.86

Et ceux annotés par la base de données générées par REPET	A01 Brapa_REPET_TEs match 23301989 23302125 0.0 – . ID=ms50497_A01_RLX-incomp_MCL4_Brapa-B-R9322-Map5_reversed;Target=RLX-incomp_MCL4_Brapa-B-R9322-Map5_reversed 1905 2042;TargetLength=2846;Identity=92.8

On a peu d’informations sur ces ETs

On a extrait les séquences des Retro Transposases dans le but de faire une phylogénie de ces séquences ce qui devrait nous indiquer les familles des différents ETs (plutôt avec du Neighbour-Joining, vu qu’on a beaucoup de séquences).
