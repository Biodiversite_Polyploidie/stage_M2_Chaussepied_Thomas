#!/bin/bash/

## AIMS    : to cut in half fasta file to detect REPET Blaster errors
## USAGE   : ./split_batch.sh file.fa
## NOTE    : 
## AUTHORS : thomas.chaussepied@wanadoo.fr

###############################################################################

batch=$1
grep '>' $batch > tmp 
nb_line=`wc -l tmp | cut -f 1 -d ' '`
middle_line=$((nb_line/2))

middle_chunk=`head -$middle_line tmp | tail -1`

rm -rf tmp 

middle_chunk_line=`grep -n $middle_chunk $batch | cut -f 1 -d ':'`

split -$((middle_chunk_line-1)) $batch

if [ -e 'xac']
then 
	cat xac >> xab
	rm -rf xac
fi

mv xaa a_$batch
mv xab b_$batch

###############################################################################
