#!/bin/bash

# shell to use
#$ -S /bin/bash

#$ -M thomas.chaussepied@wanadoo.fr
# send mail before b, at the end e, on stop a, on suspend s, or never n
#$ -m bea
 

# if the script should be run in the current directory
#$ -cwd



## AIMS    : to keep first 'word' in line name in order to run REPET
## USAGE   : ./cleanNameLine.sh file.fa
## NOTE    : /!\ file without extension
## AUTHORS : thomas.chaussepied@wanadoo.fr


###############################################################################

path='path/of/file/'

file='file_name_without_extension'
newfile='new_file_name_without_extension'

extension='.extension'

while read line; do	
	if [[ "${line::1}" == ">" ]] ; then
		echo "${line%% *}" >> $path$newfile$extension
	else 
		echo "${line}" >> $path$newfile$extension
	fi
done < $path$file$extension

###############################################################################
