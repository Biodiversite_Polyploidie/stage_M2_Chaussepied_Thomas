#!/bin/bash

# shell to use
#$ -S /bin/bash

#$ -M thomas.chaussepied@wanadoo.fr
# send mail before b, at the end e, on stop a, on suspend s, or never n
#$ -m bea
#$ -N mask_100_napus

# if the script should be run in the current directory
#$ -cwd



## AIMS    : to mask low complexity sequence in order to run REPET
## USAGE   : ./mask_lowComplex.sh file.fa cut_off
## NOTE    : cut_off is an integer
## AUTHORS : thomas.chaussepied@wanadoo.fr
#$ -j y

# init env X
. /local/env/envmeme.sh

set -o nounset # exit on unset variable.
set -o errexit # exit on unexpected error.

###############################################################################

declare -r INFILE="${1}" # read input file.
declare -r mask="${2}" # read input file.

OUTFILE=`echo "${INFILE%%.*}"`"_"$mask"."`echo "${INFILE##*.}"`
###############################################################################
function printVars ()
{
        printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
        printf "╟ - SCRIPT              = $0\n"
        printf "╟ - INFILE		= $INFILE\n"
        printf "╟ - mask		= $mask\n"
        printf "╟ - OUTFILE             = $OUTFILE\n"
        printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

printVars >&2

###############################################################################

dust $INFILE $mask > $OUTFILE

###############################################################################
# your work.
printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0
