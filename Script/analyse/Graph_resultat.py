
# coding: utf-8

# Chargement des packages

# In[1]:

import pandas as pd
import matplotlib.pyplot as plt
import pylab
import matplotlib.patches as mpatches
import string
import os
import numpy as np
from pandas.tools.plotting import table
import math


# Fonctions créées

# pour la préparation des datas

# In[2]:

def drop_heading(frame):
    """to remove line with ## in file.ggf3"""
    drop_heading = []
    for i in range(len(frame)):
        if "##" in frame.iloc[i,0]:
            drop_heading.append(i)
    return frame.drop(drop_heading)


# In[3]:

def get_chr_length(data):
    sum = 0
    for i in range(1,len(data)):
        length = 0
        if data.iloc[i,0].split(' ')[0] == "##sequence-region":
            beg = data.iloc[i,0].split(' ')[-2]
            end = data.iloc[i,0].split(' ')[-1]
            length = int(end) - int(beg)
        else:
                   return sum
        sum += length
    return sum


# In[4]:

def get_TE_length(data,i):
    """length = end - begin"""
    if i+1 < len(data):
        if int(data.iloc[i+1,3]) > int(data.iloc[i,4]):
            if int(data.iloc[i,4]) > int(data.iloc[i,3]):
                lg = int(data.iloc[i,4]) - int(data.iloc[i,3])
            else: 
                lg = int(data.iloc[i,3]) - int(data.iloc[i,4])
        else:
            if int(data.iloc[i,4]) > int(data.iloc[i,3]):
                if int(data.iloc[i+1,4]) > int(data.iloc[i+1,3]):
                    lg = (int(data.iloc[i,4]) - int(data.iloc[i,3])) - (int(data.iloc[i+1,4]) - int(data.iloc[i+1,3]))
                else:
                    lg = (int(data.iloc[i,4]) - int(data.iloc[i,3])) - (int(data.iloc[i+1,3]) - int(data.iloc[i+1,4]))
            else:
                if int(data.iloc[i+1,4]) > int(data.iloc[i+1,3]):
                    lg = (int(data.iloc[i,3]) - int(data.iloc[i,4])) - (int(data.iloc[i+1,4]) - int(data.iloc[i+1,3]))
                else:
                    lg = (int(data.iloc[i,3]) - int(data.iloc[i,4])) - (int(data.iloc[i+1,3]) - int(data.iloc[i+1,4]))
    else:
        lg = 0
    return lg


# In[5]:

def def_statut(data,i):
    """define the status of the TE"""
    statut = data.iloc[i,1]
    if not pd.isnull(statut): 
        if "REPET_TEs" in statut:
            return "no_annot"
        elif "REPET_SSRs" in statut:
            return "SSR"
        elif "REPET_blastx" in statut or "REPET_tblastx" in statut:
            return "annot"
        else:
            return "other"


# In[6]:

def is_comp(line):
    """is the TE complete"""
    if "incomp" in line:
        return "imcomp"
    elif "comp" in line:
        return "comp"
    else:
        return "noCat"


# In[7]:

def is_chim(line):
        return ("chim" in line)


# In[8]:

def define_class(line):
    """give the TE class """
    class_I = [ "ClassI" , "RXX" , "RYX" , "RIX" , "RLX" , "RSX" ]
    class_II = [ "ClassII" , "DXX" , "DYX" , "DHX" , "DMX" , "DTX" ]
    for i in class_I:
        if i in line:
            return "I"
    for i in class_II:
        if i in line:   
            return "II"
    if "noCat" in line:
        return "noCat"
    else:
        return "SSR"


# In[9]:

def define_type_annot(line):
    """define type of annoted TE"""
    if (line[-2]) == "?":
        # sometime there is '?' as type, impose noCat for this case
        return "noCat"
    else:
        return line[-2]


# In[10]:

def define_type_SSR():
        return "SSR"


# In[11]:

def define_type_no_annot(line):
    """define type of non annoted TE,
    turn REPET acronym into standard annotation name """
    type_name = {
        "RYX" : "DIRS",
        "RIX" : "LINE",
        "RLX" : "LTR",
        "RSX" : "SINE",
        "DYX" : "Crypton",
        "DHX" : "Helitron",
        "DMX" : "Maverick",
        "DTX" : "TIR",
        "TRIM": "TRIM",
        "LARD": "LARD",
        "MITE": "MITE",
        "?" : "noCat",
        "noCat" : "noCat"
    }
    for i in line:
        if i in type_name.keys():
            return type_name[i]
        else:
            continue
    return "noCat"


# In[12]:

def define_name( statut , i , line ):
    if statut[i] == "annot":
        if line[-1] == "?":
            return "noCat"
        else:
            return line[-1]
    else:
        return "noCat"


# In[13]:

def gff3_to_graph(data):
    """prepare the dataframe for the graph stage"""
    statut = []
    chim = []
    comp = []
    class_TE = []
    type_TE = []
    name_TE = []
    length_TE = []

    for i in range(len(data)):
        statut.append(def_statut(data,i))
        length_TE.append(get_TE_length(data,i))
        
        # informations are not homogenized for the different status
        # each statu is treated in a specific way
        # line is the information column
        if statut[i] == "no_annot":
            # ID=mp189122-1_A01_DTX-incomp_MCL12_Brapa-B-R2443-Map8;
                    # Parent=ms189122_A01_DTX-incomp_MCL12_Brapa-B-R2443-Map8;
                    # Target=DTX-incomp_MCL12_Brapa-B-R2443-Map8 424 601;Identity=84.5
            # [DTX, incomp]
            line = (data.iloc[i,8].strip("ID=").split(";")[0].split('_')[2].split('-'))
            type_TE.append(define_type_no_annot(line))
        elif statut[i] == "SSR":
            line = (data.iloc[i,8].strip("ID=").split(";")[0].split('_')[2].split('-'))
            type_TE.append(define_type_SSR())
        elif statut[i]== "annot":
            # ID=ms32756_A01_Gypsy-52_Mad-I_1p:ClassI:LTR:Gypsy;
                    # Target=Gypsy-52_Mad-I_1p:ClassI:LTR:Gypsy 869 881;
                    # TargetLength=1893;Identity=35.23
            # [ms32756_A01_Gypsy-52_Mad-I_1p, ClassI, LTR, Gypsy]
            line = (data.iloc[i,8].strip("ID=").split(";")[0].split(':'))
            type_TE.append(define_type_annot(line))
        else:
            continue
        comp.append(is_comp(line))
        chim.append(is_chim(line))
        class_TE.append(define_class(line))
        name_TE.append(define_name(statut, i , line))

    data["statut"] = statut
    data["comp"] = comp
    data["chim"] = chim
    data["class_TE"] = class_TE
    data["type_TE"] = type_TE
    data["name"] = name_TE
    data["length"] = length_TE
    return data


# pour la préparation des graphs

# In[14]:

def move_noCat(ident_dic, dict_list):
    """"To put noCat element on the end of its group"""
    if "noCat" in dict_list.get(ident_dic)[0]:
        i = dict_list.get(ident_dic)[0].index("noCat")
        TE_i = dict_list.get(ident_dic)[0][i]
        nb_i = dict_list.get(ident_dic)[1][i]
        col_i = dict_list.get(ident_dic)[2][i]

        del dict_list.get(ident_dic)[0][i]
        del dict_list.get(ident_dic)[1][i]
        del dict_list.get(ident_dic)[2][i]

        return dict_list.get(ident_dic)[0].append(TE_i),     dict_list.get(ident_dic)[1].append(nb_i),     dict_list.get(ident_dic)[2].append(col_i)


# In[15]:

def fill_prep(ident_dic, dict_list):
    """sort id, fill preparation lists"""
    id_sorted = sorted(dict_list.get(ident_dic)[-1])
    for i in id_sorted:
        dict_list.get(ident_dic)[0].append(i) # name
        dict_list.get(ident_dic)[1].append(len( dict_list.get(ident_dic)[4]                                               [ (dict_list.get(ident_dic)[4].type_TE == i) ])) # nb
        dict_list.get(ident_dic)[2].append(class_color(ident_dic, i)) # color
        dict_list.get(ident_dic)[3].append(0.2) # explode

    return dict_list


# In[16]:

def class_color(ident_dic, i):
    """choose the color for the graph"""
    if ident_dic == "I":
        if i == 'noCat':
            return 'pink'
        else:
            return 'red'
    elif ident_dic == "II":
        if i == 'noCat':
            return 'purple'
        else:
            return 'blue'
    elif ident_dic == "SSR":
        return 'yellow'
    else:
        return 'green'


# In[17]:

def prep_graph(ref_frame):
    """preparation of the list used by the graph"""
    name_I = list(set(ref_frame [ ref_frame.class_TE == 'I']['type_TE']))
    name_II = list(set(ref_frame [ ref_frame.class_TE == 'II']['type_TE']))
    name_noCat = list(set(ref_frame [ ref_frame.class_TE == 'noCat']['type_TE']))
    name_SSR = list(set(ref_frame [ ref_frame.class_TE == 'SSR']['type_TE']))

    
    ref_I = ref_frame [ (ref_frame.class_TE == 'I') ]
    ref_II = ref_frame [ (ref_frame.class_TE == 'II') ]
    ref_noCat = ref_frame [ (ref_frame.class_TE == 'noCat') ]
    ref_SSR = ref_frame [ (ref_frame.class_TE == 'SSR') ]

    nb = []
    nb_I = []
    nb_II = []
    nb_noCat = []
    nb_SSR = []

    name_TE = []
    name_TE_I = []
    name_TE_II = []
    name_TE_noCat = []
    name_TE_SSR = []

    color = []
    color_I = []
    color_II = []
    color_noCat = []
    color_SSR = []

    explode = []
    explode_I = []
    explode_II = []
    explode_noCat = []
    explode_SSR = []

    dict_list = {
    "I" : [ name_TE_I, nb_I, color_I, explode_I , ref_I , name_I ],
        "II" : [ name_TE_II, nb_II, color_II, explode_II , ref_II, name_II ],
        "noCat" : [ name_TE_noCat, nb_noCat, color_noCat , explode_noCat , ref_noCat, name_noCat ],
        "SSR" : [ name_TE_SSR, nb_SSR, color_SSR, explode_SSR , ref_SSR , name_SSR ]
        }
    dict_list = {
		"I" : [ name_TE_I, nb_I, color_I, explode_I , ref_I , name_I ],
		"II" : [ name_TE_II, nb_II, color_II, explode_II , ref_II, name_II ],
		"noCat" : [ name_TE_noCat, nb_noCat, color_noCat , explode_noCat , ref_noCat, name_noCat ]
	}
    
    for i in dict_list.keys():
        fill_prep(i,dict_list)
        move_noCat(i,dict_list)

    nb = nb_I + nb_II + nb_noCat + nb_SSR
    name_TE = name_TE_I + name_TE_II + name_TE_noCat + name_TE_SSR
    color = color_I + color_II + color_noCat + color_SSR
    explode = explode_I + explode_II + explode_noCat + explode_SSR
    
    prep = [nb , name_TE , color , explode]

    return prep


# In[18]:

def graph(out_prep_graph, title, save ):
    """generate the graph 
    choose the title
    """
    nb = out_prep_graph[0]
    name_TE = out_prep_graph[1]
    color = out_prep_graph[2]
    explode = out_prep_graph[3]

    bar = plt.bar(range(len(name_TE)),
            nb,
            color = color)
    
    # add name labels on individual bars
    pylab.xticks(range(len(name_TE)), 
                 name_TE, 
                 rotation = 60)

    for rect in bar:
        # add height labels on individual bars
        height = rect.get_height()
        plt.text(rect.get_x() + rect.get_width()/2.0, height,
                 '%d' % int(height), 
                 ha='center', 
                 va='bottom',
                fontsize=6 )
    
    # add a legend
    patch_I = mpatches.Patch(color='red', label='Class I')
    patch_I_noCat = mpatches.Patch(color='pink', label='Class I')
    patch_II = mpatches.Patch(color='blue', label='Class II')
    patch_II_noCat = mpatches.Patch(color='purple', label='Class II')
    patch_noCat = mpatches.Patch(color='green', label='no class')
    patch_SSR = mpatches.Patch(color='yellow', label='SSR')

    plt.legend(handles=[patch_I, patch_I_noCat, patch_II, patch_II_noCat, patch_noCat],
               bbox_to_anchor=(1.05, 1),
               loc=2,
               borderaxespad=0.)
    #plt.legend(handles=[patch_I, patch_I_noCat, patch_II, patch_II_noCat, patch_noCat, patch_SSR],
    #           bbox_to_anchor=(1.05, 1),
    #           loc=2,
    #           borderaxespad=0.)

    # to avoid too small graph 
    xmax = len(name_TE)
    #ymax = plt.gca().get_ybound()
    #ymax = ymax[1]*1.1 
    ymax = 500000
    plt.axis([-0.5, xmax-0.5, 0, ymax])
    
    plt.title(title)

    if save == 1:
        plt.savefig("/home/adminigepp/Bureau/Stage_ET/Results/Pictures/Quantification/{0}.png".format(title.replace(" ", "_")), format = "png", bbox_inches='tight', dpi = 200 ) 
        
    plt.show()


# In[19]:

def graph_TE_in_data(path, save = 0):
    """generate graph for all files in a folder
    you can save the graph -> save = 1"""
    for dossier, sous_dossiers, fichiers in os.walk(path):
        for fichier in fichiers:
            if  fichier.endswith(".gff3"):
                names = ["chr" , "detection" , "match" , "start" , "end" , "score" , "strand" , "phase" , "information"]
            elif fichier.endswith(".classif"):
                names = ["name" , "size" , "strand" , "clear" , "class_TE" , "type_TE" , "category_TE" , "description"]

            else:
                print("ERROR with : ",(os.path.join(dossier, fichier)))
                continue

            print("BEGIN with : ",(os.path.join(dossier, fichier)))

            data = pd.read_csv((os.path.join(dossier, fichier)), sep='\t', names = names)
            data_frame = pd.DataFrame(data)
            data_frame = drop_heading(data_frame)

            print("Data frame is ready \o/")
            if ".gff3" in fichier:
                data_frame = gff3_to_graph(data_frame)
            print("Graph preparation")
            prep_data = prep_graph(data_frame)
            print("Et voilà")
            graph(prep_data, "TEs in {0} {1}".format(dossier.split('/')[-2],fichier.split('.')[0]), save = save)


# In[20]:

def give_length_TE(data_frame):
    length_I = 0
    length_II = 0
    length_SSR = 0
    length_noCat = 0
    lengths = []
    for i in range(len(data_frame.groupby(['class_TE','type_TE']).sum())):
        class_TE = data_frame.groupby(['class_TE','type_TE']).sum().axes[0].tolist()[i][0]
        length_TE = int(data_frame.groupby(['class_TE','type_TE']).sum().iloc[i,4])
        if class_TE == 'I' : 
            length_I += length_TE
        elif class_TE == 'II' : 
            length_II += length_TE
        elif class_TE == 'SSR' : 
            length_SSR += length_TE
        elif class_TE == 'noCat' : 
            length_noCat += length_TE
    lengths = [length_I, length_II, length_SSR, length_noCat]
    return lengths


# In[21]:

def prep_data_TE_cover (path):
    for dossier, sous_dossiers, fichiers in os.walk(path):
        class_I = []
        class_II = []
        noCat = []
        SSR = []
        Chr = []
        names_chr = []
        for fichier in sorted(fichiers):
            #sorted orders files
            if ".gff3" in fichier:
                names = ["chr" , "detection" , "match" , "start" , "end" , "score" , "strand" , "phase" , "information"]
            else:
                print("ERROR with : ",(os.path.join(dossier, fichier)))
                continue
            print("We are working with : ", fichier)
            data = pd.read_csv(os.path.join(dossier, fichier), sep='\t', names = names)
            length_chr = get_chr_length(data)
            data_frame = pd.DataFrame(data)
            data_frame = drop_heading(data_frame)
            data_frame = data_frame.sort_values(by=["start","end"])
            data_frame = gff3_to_graph(data_frame)
            print("Data frame is ready")

            lengths = give_length_TE(data_frame)

            class_I.append(lengths[0])
            class_II.append(lengths[1])
            noCat.append(lengths[3])
            SSR.append(lengths[2])
            Chr.append(length_chr) 
            names_chr.append(fichier.split('.')[0])
            print(fichier,"'s datas are ready for the graph step !")
            prep_graph_TE_cover = [class_I, class_II, noCat, SSR, Chr, names_chr ]
        return prep_graph_TE_cover
    


# In[22]:

def graph_TE_cover(path, save = 0):
    print("Files in ", path, "are parsing")

    prep_graph_TE_cover = prep_data_TE_cover(path)

    print("Now let's begin the graph \o/")

    class_I = [x / 10**(6) for x in prep_graph_TE_cover[0]]
    class_II = [x / 10**(6) for x in prep_graph_TE_cover[1]]
    noCat = [x / 10**(6) for x in prep_graph_TE_cover[2]]
    SSR = [x / 10**(6) for x in prep_graph_TE_cover[3]]
    Chr = [x / 10**(6) for x in prep_graph_TE_cover[4]]
    names_chr = prep_graph_TE_cover[5]


    barWidth = 0.8
    bottom_II = class_I
    bottom_noCat = [bottom_II[i]+class_II[i] for i in range(min(len(bottom_II),len(class_II)))]+max(bottom_II,class_II,key=len)[min(len(bottom_II),len(class_II)):]
    bottom_SSR = [bottom_noCat[i]+noCat[i] for i in range(min(len(bottom_noCat),len(noCat)))]+max(bottom_noCat,noCat,key=len)[min(len(bottom_noCat),len(noCat)):]

    r = range(len(class_I))
    #r_chr = [x + barWidth for x in r]

    plt.bar(r, Chr, width = barWidth, color = '#e2d3e0')
    plt.bar(r, class_I, width = barWidth, color = 'red')
    plt.bar(r, class_II, bottom = bottom_II, width = barWidth, color = 'blue')
    plt.bar(r, noCat, bottom = bottom_noCat, width = barWidth, color = 'green')
    plt.bar(r, SSR, bottom = bottom_SSR, width = barWidth, color = 'yellow')

    patch_I = mpatches.Patch(color='red', label='Class I')
    patch_II = mpatches.Patch(color='blue', label='Class II')
    patch_noCat = mpatches.Patch(color='green', label='no class')
    patch_SSR = mpatches.Patch(color='yellow', label='SSR')
    patch_Chr = mpatches.Patch(color='#e2d3e0', label='no TE')

    plt.legend(handles=[patch_I, patch_II, patch_noCat, patch_SSR, patch_Chr],
           bbox_to_anchor=(1.05, 1),
           loc=2,
           borderaxespad=0.)

    plt.xticks([r + barWidth /2 for r in range(len(Chr))], names_chr, rotation = 60)

    xmax = len(Chr)
    ymax = plt.gca().get_ybound()
    ymax = ymax[1]*1.1
    plt.axis([0, xmax, 0, ymax])

    plt.ylabel("length in Mb")
    
    for dossier in os.walk(path):
        title = "TEs cover on {0} chromosomes".format(dossier[0].split('/')[-2])
    plt.title(title)

    if save == 1:
        plt.savefig("/root/jupyter/shared/data/Pictures/{0}.png".format(title.replace(" ", "_")), format = "png", bbox_inches='tight', dpi = 200 )

    plt.show()


# In[23]:

#names = ["chr" , "detection" , "match" , "start" , "end" , "score" , "strand" , "phase" , "information"]
#data = pd.read_csv(os.path.join("/root/jupyter/shared/data/Brapa/test/A01.gff3"), sep='\t', names = names)
#data1 = pd.read_csv(os.path.join("/root/jupyter/shared/data/Brapa/test/A01.gff3"), sep='\t', names = names)

#data_frame = pd.DataFrame(data)
#data1_frame = pd.DataFrame(data1)
#data_frame = data_frame.sort_values(by=["start","end"])
#data_frame = data_frame.dropna()
#data1_frame = (data1_frame.sort_values(by=["start","end"]))
#data_frame = drop_heading(data_frame)
#data1_frame = drop_heading(data1_frame)
#data_frame = gff3_to_graph(data_frame)
#data1_frame = gff3_to_graph(data1_frame)


# In[24]:

#data1_frame = gff3_to_graph(data_frame)
#print(data_frame.sort_values(by=["start","end"]))


# In[25]:

#test = (data_frame.sort_values(by=["start","end"]))
#test1 = (data1_frame.sort_values(by=["start","end"]))

#print(test)
#def remove_TE_in_TE(data_frame,i):
#    if int(data_frame.iloc[i,4]) > int(data_frame.iloc[i+1,3]):
 #       return data_frame.iloc[i+1,15]
#    else: return 0


#print(test.groupby(['class_TE','type_TE']).sum())
#print(test1.groupby(['class_TE','type_TE']).sum())


# Chargement des datas

# Traitement des datas

# In[26]:

#graph_TE_in_data("/root/jupyter/shared/repet-disk/Bn_m_100_wo_r_c/result/Bn_m_100_wo_r_c", save = 0)


# Graph : Barplot nombre des différents types d'ET par type et par classe (noCat = non categorisé)

# In[ ]:




# In[ ]:




# In[27]:

def get_TE_number(data):
    return len(data)-2 # - 2 headers


# In[28]:

def get_TE_chrom_Mb(data,len_chr):
    """get TEs per Mb on chrom"""
    return (get_TE_number(data)/len_chr)*(10**6)


# In[29]:

def get_TE_window(data,beg,end):
    #print(beg, " ", end)
    return data[(data['start'] >= beg)  & (data['end'] < end)].shape[0]


# In[30]:

def sliding_window_ratio_TE(data,len_chr):    
    beg = 0 
    end = 2*(10**6)
    nb_TE = 0
    TE_chr=get_TE_chrom_Mb(data,len_chr)
    ratio_TE=[]
    while end <= (len_chr):
        ratio_TE.append((((get_TE_window(data,beg,end)/(end-beg)*(10**6)/TE_chr))-1))
        beg += 0.2*(10**6)
        end += 0.2*(10**6)
    #end = get_chr_length(data)
    #ratio_TE.append((((get_TE_window(data,beg,end)/(end-beg)*(10**6)/TE_chr)*100)-100))
    return ratio_TE


# In[31]:

def position_centromere(path):
    #'/root/jupyter/shared/repet-disk/banks/genome/position_centro_Brap.txt'
    with open(path, "r") as f:
        # open in a list centromere positions 
                        lines = [line.strip('\n') for line in f.readlines()]
                        pos_centro = {}
                        for line in lines: 
                            pos_centro[line.split(' ')[0]] = (line.split(' ')[1:])
    return pos_centro


# In[32]:

def graph_distribution_TE_on_chr(path, dico, save = 0):

    for dossier, sous_dossiers, fichiers in os.walk(path):
        for fichier in fichiers:
            if  fichier.endswith(".gff3"):
                names = ["chr" , "detection" , "match" , "start" , "end" , "score" , "strand" , "phase" , "information"]
            else:
                print("ERROR with : ",(os.path.join(dossier, fichier)))
                continue
            if "Scaffold" in fichier:
                print("not for you, scaffold :'(")
                continue
            print("BEGIN with : ",(os.path.join(dossier, fichier)))
            data = pd.read_csv(os.path.join(dossier, fichier), sep='\t', names = names)

            data_frame = pd.DataFrame(data)
            print("Dataframe : OK")
            data_frame = drop_heading(data_frame)
            print("la tête est tombée")
            data_frame = data_frame.sort_values(by=["start","end"])
            print("les ETs sont rangés")
            data_frame_graf = gff3_to_graph(data_frame)
            print("Data frame is ready")
            
            if dico == "Brapa":
                dic_pos_centro = position_centromere(
                    '/root/jupyter/shared/repet-disk/banks/genome/position_centro_Brap.txt'
                )
            elif dico == "Boleracea":
                dic_pos_centro = position_centromere(
                    '/root/jupyter/shared/repet-disk/banks/genome/position_centro_Bol.txt'
                )
            elif dico == "Bnapus":
                dic_pos_centro = position_centromere(
                    '/root/jupyter/shared/repet-disk/banks/genome/position_centro_Bnap.txt'
                )
            else:
                continue
            print("Your dictionary is : ", dico)
            pos_centro = dic_pos_centro[fichier.split('.')[0].split('_')[0]]

            len_chr = get_chr_length(data)

            #sub_data1 = data_frame_graf[data_frame_graf['class_TE'] == "I"]
            #sub_data2= data_frame_graf[data_frame_graf['class_TE'] == "II"]
            #sub_data3 = data_frame_graf[data_frame_graf['class_TE'] == "SSR"]
            
            #patch_I = mpatches.Patch(color='red', label='Class I')
            #patch_II = mpatches.Patch(color='blue', label='Class II')
            #patch_SSR = mpatches.Patch(color='yellow', label='SSR')
            
            sub_data1 = data_frame_graf[data_frame_graf['class_TE'] == "I"]
            sub_data2 = data_frame_graf[data_frame_graf['type_TE'] == "LTR"]
            #sub_data3= data_frame_graf[data_frame_graf['name'] == "Gypsy"]
            #sub_data4 = data_frame_graf[data_frame_graf['name'] == "Copia"]
            
            sub_data5= data_frame_graf[data_frame_graf['class_TE'] == "II"]
            #sub_data6 = data_frame_graf[data_frame_graf['type_TE'] == "Crypton"]
            sub_data7 = data_frame_graf[data_frame_graf['type_TE'] == "Helitron"]
            #sub_data8 = data_frame_graf[data_frame_graf['type_TE'] == "Maverick"]
            sub_data9 = data_frame_graf[data_frame_graf['type_TE'] == "TIR"]
#             sub_data10 = data_frame_graf[data_frame_graf['name'] == "CACTA"]
#             sub_data11 = data_frame_graf[data_frame_graf['type_TE'] == "MITE"]

            patch_1 = mpatches.Patch(color='#ff0000', label='Class I')
            patch_2 = mpatches.Patch(color='#ff8000', label='LTR')
#             patch_3 = mpatches.Patch(color='#ff0080', label='Gypsy')
#             patch_4 = mpatches.Patch(color='#ff66b2', label='Copia')
            
            patch_5 = mpatches.Patch(color='#0000ff', label='Class II')
            #patch_6 = mpatches.Patch(color='#8000ff', label='Crypton')
            patch_7 = mpatches.Patch(color='#af95ed', label='Helitron')
            #patch_8 = mpatches.Patch(color='#0080ff', label='Marverick')
            patch_9 = mpatches.Patch(color='#21a826', label='TIR')
#             patch_10 = mpatches.Patch(color='#87e5c2', label='Cacta')
#             patch_11 = mpatches.Patch(color='#ccccff', label='MITE')

            ## you can choose what you want 
            #sub_data = data_frame_graf[data_frame_graf['type_TE'] == "LTR"]
            #sub_data = data_frame_graf[data_frame_graf['name'] == "Gypsy"]

            Class1 = sliding_window_ratio_TE(sub_data1,len_chr)
            Class2 = sliding_window_ratio_TE(sub_data2,len_chr)
#             Class3 = sliding_window_ratio_TE(sub_data3,len_chr)
#             Class4 = sliding_window_ratio_TE(sub_data4,len_chr)
            Class5 = sliding_window_ratio_TE(sub_data5,len_chr)
            #Class6 = sliding_window_ratio_TE(sub_data6,len_chr)
            Class7 = sliding_window_ratio_TE(sub_data7,len_chr)
            #Class8 = sliding_window_ratio_TE(sub_data8,len_chr)
            Class9 = sliding_window_ratio_TE(sub_data9,len_chr)
#             Class10 = sliding_window_ratio_TE(sub_data10,len_chr)
#             Class11 = sliding_window_ratio_TE(sub_data11,len_chr)

            ## you have to select the same sub data
            #LTR = sliding_window_ratio_TE(sub_data_LTR,len_chr)
            #Gypsy = sliding_window_ratio_TE(sub_data_gypsy,len_chr)

            step = len(Class1)
            x_values = np.linspace(0,len_chr/10**(6),step)

            #fig, (ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8,ax9,ax10,ax11) = plt.subplots(11, 1, sharex=True,figsize=(15,15))
            fig, (ax1,ax2,ax5,ax7,ax9) = plt.subplots(5, 1, sharex=True,figsize=(10,10))
            
            ax1.axhline(y=0, color='black', linestyle='-')
            ax2.axhline(y=0, color='black', linestyle='-')
#             ax3.axhline(y=0, color='black', linestyle='-')
#             ax4.axhline(y=0, color='black', linestyle='-')
            ax5.axhline(y=0, color='black', linestyle='-')
            #ax6.axhline(y=0, color='black', linestyle='-')
            ax7.axhline(y=0, color='black', linestyle='-')
            #ax8.axhline(y=0, color='black', linestyle='-')
            ax9.axhline(y=0, color='black', linestyle='-')
#             ax10.axhline(y=0, color='black', linestyle='-')
#             ax11.axhline(y=0, color='black', linestyle='-')

            #Class_tot = Class1 + Class2 + Class3 + Class4 + Class5 + Class6 + Class7 + Class8 + Class9 + Class10 + Class11
            Class_tot = Class1 + Class2 + Class5 + Class7 + Class9 
            ax1.fill_between([int(pos_centro[0])/10**(6),
                              int(pos_centro[1])/10**(6)],
                             min(Class_tot)*1.1,
                             max(Class_tot)*1.1,
                             facecolor='white',
                             hatch='/')
            ax2.fill_between([int(pos_centro[0])/10**(6),
                              int(pos_centro[1])/10**(6)],
                             min(Class_tot)*1.1,
                             max(Class_tot)*1.1,
                             facecolor='white',
                             hatch='/')
#             ax3.fill_between([int(pos_centro[0])/10**(6),
#                               int(pos_centro[1])/10**(6)],
#                              min(Class_tot)*1.1,
#                              max(Class_tot)*1.1,
#                              facecolor='white',
#                              hatch='/')
#             ax4.fill_between([int(pos_centro[0])/10**(6),
#                               int(pos_centro[1])/10**(6)],
#                              min(Class_tot)*1.1,
#                              max(Class_tot)*1.1,
#                              facecolor='white',
#                              hatch='/')
            ax5.fill_between([int(pos_centro[0])/10**(6),
                              int(pos_centro[1])/10**(6)],
                             min(Class_tot)*1.1,
                             max(Class_tot)*1.1,
                             facecolor='white',
                             hatch='/')
#             ax6.fill_between([int(pos_centro[0])/10**(6),
#                               int(pos_centro[1])/10**(6)],
#                              min(Class_tot)*1.1,
#                              max(Class_tot)*1.1,
#                              facecolor='white',
#                              hatch='/')
            ax7.fill_between([int(pos_centro[0])/10**(6),
                              int(pos_centro[1])/10**(6)],
                             min(Class_tot)*1.1,
                             max(Class_tot)*1.1,
                             facecolor='white',
                             hatch='/')
#             ax8.fill_between([int(pos_centro[0])/10**(6),
#                               int(pos_centro[1])/10**(6)],
#                              min(Class_tot)*1.1,
#                              max(Class_tot)*1.1,
#                              facecolor='white',
#                              hatch='/')
            ax9.fill_between([int(pos_centro[0])/10**(6),
                              int(pos_centro[1])/10**(6)],
                             min(Class_tot)*1.1,
                             max(Class_tot)*1.1,
                             facecolor='white',
                             hatch='/')
#             ax10.fill_between([int(pos_centro[0])/10**(6),
#                               int(pos_centro[1])/10**(6)],
#                              min(Class_tot)*1.1,
#                              max(Class_tot)*1.1,
#                              facecolor='white',
#                              hatch='/')
#             ax11.fill_between([int(pos_centro[0])/10**(6),
#                               int(pos_centro[1])/10**(6)],
#                              min(Class_tot)*1.1,
#                              max(Class_tot)*1.1,
#                              facecolor='white',
#                              hatch='/')
            
        #plt.plot(x_values,Gypsy, color='yellow')
        #plt.plot(x_values,LTR, color='orange')
            #print(len(x_values))
            #print(len(Class))
            #plt.plot(x_values,Class, color='red')


        #plt.fill_between(x_values,0,Gypsy, color='yellow')
        #plt.fill_between(x_values,0,LTR, color='orange')
            ax1.fill_between(x_values,0,Class1, color='#ff0000')
            ax2.fill_between(x_values,0,Class2, color='#ff8000')
#             ax3.fill_between(x_values,0,Class3, color='#ff0080')
#             ax4.fill_between(x_values,0,Class4, color='#ff66b2')
            
            ax5.fill_between(x_values,0,Class5, color='#0000ff')
            #ax6.fill_between(x_values,0,Class6, color='#8000ff')
            ax7.fill_between(x_values,0,Class7, color='#af95ed')
            #ax8.fill_between(x_values,0,Class8, color='#0080ff')
            ax9.fill_between(x_values,0,Class9, color='#21a826')
#             ax10.fill_between(x_values,0,Class10, color='#87e5c2')
#             ax11.fill_between(x_values,0,Class11, color='#ccccff')

            
            plt.xlim([0,max(x_values)])
            ax1.set_ylim([min(Class_tot)*1.1,
                          max(Class_tot)*1.1])
            ax2.set_ylim([min(Class_tot)*1.1,
                          max(Class_tot)*1.1])
#             ax3.set_ylim([min(Class_tot)*1.1,
#                           max(Class_tot)*1.1])
#             ax4.set_ylim([min(Class_tot)*1.1,
#                           max(Class_tot)*1.1])
            ax5.set_ylim([min(Class_tot)*1.1,
                          max(Class_tot)*1.1])
#             ax6.set_ylim([min(Class_tot)*1.1,
#                           max(Class_tot)*1.1])
            ax7.set_ylim([min(Class_tot)*1.1,
                          max(Class_tot)*1.1])
#             ax8.set_ylim([min(Class_tot)*1.1,
#                           max(Class_tot)*1.1])
            ax9.set_ylim([min(Class_tot)*1.1,
                          max(Class_tot)*1.1])
#             ax10.set_ylim([min(Class_tot)*1.1,
#                           max(Class_tot)*1.1])
#             ax11.set_ylim([min(Class_tot)*1.1,
#                           max(Class_tot)*1.1])
                
            
            #plt.legend(handles=[patch_1, patch_2, patch_3, patch_4, patch_5, patch_6, patch_7, patch_8, patch_9, patch_10, patch_11],loc='upper center', bbox_to_anchor=(0.5, -1),ncol =11)
            plt.legend(handles=[patch_1, patch_2, patch_5, patch_7, patch_9],loc='upper center', bbox_to_anchor=(0.5, -1),ncol =11)

            title = "Distribution of ratio of TEs families and sub families density along {0} chromosome {1}".format(dossier.split('/')[-1],fichier.split('.')[0].split('_')[0])
            plt.suptitle(title,horizontalalignment='center', verticalalignment='bottom')
            plt.xlabel("Mb")
            plt.show()
            if save == 1:
                fig.savefig("/root/jupyter/shared/data/Pictures/{0}.png".format(title.replace(" ", "_")),
                            format = "png", bbox_inches='tight', dpi = 200 )


# In[33]:


#graph_distribution_TE_on_chr('/root/jupyter/shared/data/Brapa/Brapa_wo_match_part', "Brapa", save = 1)
#graph_distribution_TE_on_chr('/root/jupyter/shared/data/Brapa/Brapa_wo_match_part/Brapa_wo_match_part_comp', "Brapa", save = 1)
#graph_distribution_TE_on_chr('/root/jupyter/shared/data/Brapa/Brapa_wo_match_part/Brapa_wo_match_part_incomp', "Brapa", save = 1)

#graph_distribution_TE_on_chr('/root/jupyter/shared/repet-disk/Boleracea_mask/result/Boleracea_mask/Boleracea_dust_100_wo_match_part', "Boleracea", save = 1)
#graph_distribution_TE_on_chr('/root/jupyter/shared/repet-disk/Boleracea_mask/result/Boleracea_mask/Boleracea_dust_100_wo_match_part/Boleracea_dust_100_wo_match_part_comp', "Boleracea", save = 1)
#graph_distribution_TE_on_chr('/root/jupyter/shared/repet-disk/Boleracea_mask/result/Boleracea_mask/Boleracea_dust_100_wo_match_part/Boleracea_dust_100_wo_match_part_incomp', "Boleracea", save = 1)

#graph_distribution_TE_on_chr('/root/jupyter/shared/repet-disk/Bn_m_100_wo_r_c/result/Bn_m_100_wo_r_c/Bnapus_dust_100_wo_repetition_centromere_and_match_part', "Bnapus", save = 1)
#graph_distribution_TE_on_chr('/root/jupyter/shared/repet-disk/Bn_m_100_wo_r_c/result/Bn_m_100_wo_r_c/Bnapus_dust_100_wo_repetition_centromere_and_match_part/Bnapus_dust_100_wo_repetition_centromere_and_match_part_TEs_comp', "Bnapus", save = 1)
#graph_distribution_TE_on_chr('/root/jupyter/shared/repet-disk/Bn_m_100_wo_r_c/result/Bn_m_100_wo_r_c/Bnapus_dust_100_wo_repetition_centromere_and_match_part/Bnapus_dust_100_wo_repetition_centromere_and_match_part_TEs_incomp', "Bnapus", save = 1)

#graph_distribution_TE_on_chr('/root/jupyter/shared/repet-disk/Brapa_wo_centro/result/Brapa_wo_centro', "Brapa",save = 1)
#graph_distribution_TE_on_chr('/root/jupyter/shared/data/Brapa_30', "Brapa",save = 1)

#graph_TE_cover('/root/jupyter/shared/data/Brapa/test/', save = 0)
#graph_TE_cover('/root/jupyter/shared/repet-disk/Brapa_wo_centro/result/Brapa_wo_centro', save = 1)
#graph_TE_in_data("/root/jupyter/shared/repet-disk/Boleracea_mask/result/Boleracea_mask", save = 1)
#graph_TE_cover('/root/jupyter/shared/repet-disk/Boleracea_mask/result/Boleracea_mask', save = 1)
## TODO
#graph_TE_cover('/root/jupyter/shared/data/Brapa', save = 1)
#graph_TE_cover('/root/jupyter/shared/data/Brapa_30', save = 1)
#graph_TE_cover('/root/jupyter/shared/repet-disk/Bn_m_100_wo_r_c/result/Bn_m_100_wo_r_c', save = 1)

#graph_TE_in_data('/root/jupyter/shared/data/Brapa/Brapa/', save = 1)
graph_TE_in_data('/home/adminigepp/Bureau/Stage_ET/Results/Analyse_data/REPET_Annotation/Brassica/', save = 1 )


