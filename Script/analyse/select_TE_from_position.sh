# shell to use
#$ -S /bin/bash
#$ -M thomas.chaussepied@wanadoo.fr
# send mail before b, at the end e, on stop a, on suspend s, or never n
#$ -m bea
# if the script should be run in the current directory
#$ -cwd
## AIMS    : to select a subset of TE annotation
## USAGE   : ./select_TE_from_position.sh position.txt annotation.gff3
## NOTE    : 
## AUTHORS : thomas.chaussepied@wanadoo.fr

set -o nounset # exit on unset variable.
set -o errexit # exit on unexpected error.

###############################################################################

declare -r INFILE_pos="${1}" # read input file.
declare -r INFILE_data="${2}" # read input file.

OUTFILE=`echo "${INFILE_data%%.*}"`"_centromere."`echo "${INFILE_data##*.}"`
###############################################################################
function printVars ()
{
        printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
        printf "╟ - SCRIPT              = $0\n"
        printf "╟ - INFILE : position   = $INFILE_pos\n"
        printf "╟ - INFILE : data       = $INFILE_data\n"
        printf "╟ - OUTFILE             = $OUTFILE\n"
        printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}
# =============================================================================
function main ()
{
        while read chr_pos beg_cen end_cen end_chr
		do
			if [[ $(cut -f1 $INFILE_data | grep $chr_pos) ]]
			then
				while read line
       			do
					if [[ $line = *'##'* ]]
					then
						echo $line >> tmp 
					fi
				
					chr=`cut -d$'\t' -f1 <<< $line`
                	beg=`cut -d$'\t' -f4 <<< $line`
                	end=`cut -d$'\t' -f5 <<< $line`
				
					if [ "$chr" == "$chr_pos" ] && [ $beg -gt $beg_cen ] && [ $end -lt $end_cen ]
					then
						echo $line >> tmp 
					fi
			done < $INFILE_data

			
			fi		
	done < $INFILE_pos 
	mv tmp $OUTFILE
}
###############################################################################

printVars >&2
# your work.
main
printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0

