Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2018-03-07T15:44:27+01:00

====== REPET-db ======

avant de relancer REPET toujours vérifier que la bd est vide 

root mer. mars 07 14:42 /mnt/repet-disk/Bnapus/
# mysql repet_db -u repet --password -h 192.168.100.83
Enter password: __password__

mysql> SHOW TABLES;
Empty set (0.00 sec) # la bd est completement vide on peut y aller

si SHOW TABLES trouve des tables ... 

mysql> SELECT * from jobs;
Empty set (0.01 sec) # la bd est vide, on peut y aller

Ctrl-C pour quitter
