Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2018-03-07T15:25:31+01:00

====== setEnv ======

 # more setEnv.sh 
	export REPET_HOST=192.168.100.83		 ## ip de la bd sql
	export REPET_USER=repet
	export REPET_PW=password
	export REPET_DB=repet_db
	export REPET_PORT=3306
	
	export REPET_PATH=/ifb/bin/repet_pipe
	export PYTHONPATH=$REPET_PATH
	export REPET_JOBS=MySQL
	export REPET_JOB_MANAGER=RABBITMQ
	export REPET_QUEUE=RABBITMQ
	export WUBLASTDIR=/ifb/WUBLAST
	export BLASTDIR=/ifb/blast-2.2.21/bin
	export PATH=$REPET_PATH/bin:$PATH
	export PATH=/ifb/bin:$BLASTDIR:$WUBLASTDIR:$PATH

